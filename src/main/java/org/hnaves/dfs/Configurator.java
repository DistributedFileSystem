package org.hnaves.dfs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configurator {
	private Configurator() {}
	
	private static final Logger LOG = Logger.getLogger(Configurator.class.getName());
	private static final int DEFAULT_UDP_PORT = 2424;
	private static final int DEFAULT_TCP_PORT = 2424;
	private static final int DEFAULT_AREYOUCOORDINATOR_TIMEOUT = 3000;
	private static final int DEFAULT_AREYOUCOORDINATOR_RANDOM_FACTOR = 2000;
	private static final int DEFAULT_AREYOUTHERE_TIMEOUT = 3000;
	private static final int DEFAULT_INVITATION_TIMEOUT = 2000;
	private static final int DEFAULT_READY_TIMEOUT = 1000;
	private static final int DEFAULT_ELECTION_TIMEOUT = 3000;
	private static final int DEFAULT_REORGANIZATION_TIMEOUT = 3000;
	private static final int DEFAULT_HTTP_START = 10000;
	private static final int DEFAULT_HTTP_PERIOD = 40000;
	private static final int DEFAULT_TRANSFER_START = 30000;
	private static final int DEFAULT_TRANSFER_PERIOD = 80000;
	private static final int DEFAULT_TRANSFER_COUNT = 3;
	private static final boolean DEFAULT_ACCEPT_TRANSFER = true;
	private static final long DEFAULT_EXPIRE_TIME = 80000;
	
	private static Properties props = null;
	
	public static int getUdpPort() {
		return getValue("UDP_PORT");
	}

	public static int getTcpPort() {
		return getValue("TCP_PORT");
	}

	public static int getAreYouCoordinatorTimeout() {
		return getValue("AREYOUCOORDINATOR_TIMEOUT");
	}

	public static int getAreYouCoordinatorRandomFactor() {
		return getValue("AREYOUCOORDINATOR_RANDOM_FACTOR");
	}

	public static int getAreYouThereTimeout() {
		return getValue("AREYOUTHERE_TIMEOUT");
	}

	public static int getInvitationTimeout() {
		return getValue("INVITATION_TIMEOUT");
	}

	public static int getReadyTimeout() {
		return getValue("READY_TIMEOUT");
	}

	public static int getElectionTimeout() {
		return getValue("ELECTION_TIMEOUT");
	}

	public static int getReorganizationTimeout() {
		return getValue("REORGANIZATION_TIMEOUT");
	}
	
	public static int getHttpStart() {
		return getValue("HTTP_START");
	}

	public static int getHttpPeriod() {
		return getValue("HTTP_PERIOD");
	}
	
	public static int getTransferStart() {
		return getValue("TRANSFER_START");
	}

	public static int getTransferPeriod() {
		return getValue("TRANSFER_PERIOD");
	}

	public static int getTransferCount() {
		return getValue("TRANSFER_COUNT");
	}

	private static int getValue(String key) {
		return Integer.parseInt(getProperties().getProperty(key));
	}
	
	public static boolean getAcceptTransfer() {
		return Boolean.parseBoolean(getProperties().getProperty("ACCEPT_TRANSFER"));
	}

	public static long getExpireTime() {
		return Long.parseLong(getProperties().getProperty("EXPIRE_TIME"));
	}

	private static void checkProperties(Properties props) {
		if (!props.containsKey("UDP_PORT")) {
			props.setProperty("UDP_PORT", Integer.toString(DEFAULT_UDP_PORT));
		}
		if (!props.containsKey("TCP_PORT")) {
			props.setProperty("TCP_PORT", Integer.toString(DEFAULT_TCP_PORT));
		}
		if (!props.containsKey("AREYOUCOORDINATOR_TIMEOUT")) {
			props.setProperty("AREYOUCOORDINATOR_TIMEOUT", Integer.toString(DEFAULT_AREYOUCOORDINATOR_TIMEOUT));
		}
		if (!props.containsKey("AREYOUCOORDINATOR_RANDOM_FACTOR")) {
			props.setProperty("AREYOUCOORDINATOR_RANDOM_FACTOR", Integer.toString(DEFAULT_AREYOUCOORDINATOR_RANDOM_FACTOR));
		}
		if (!props.containsKey("AREYOUTHERE_TIMEOUT")) {
			props.setProperty("AREYOUTHERE_TIMEOUT", Integer.toString(DEFAULT_AREYOUTHERE_TIMEOUT));
		}
		if (!props.containsKey("INVITATION_TIMEOUT")) {
			props.setProperty("INVITATION_TIMEOUT", Integer.toString(DEFAULT_INVITATION_TIMEOUT));
		}
		if (!props.containsKey("READY_TIMEOUT")) {
			props.setProperty("READY_TIMEOUT", Integer.toString(DEFAULT_READY_TIMEOUT));
		}
		if (!props.containsKey("ELECTION_TIMEOUT")) {
			props.setProperty("ELECTION_TIMEOUT", Integer.toString(DEFAULT_ELECTION_TIMEOUT));
		}
		if (!props.containsKey("REORGANIZATION_TIMEOUT")) {
			props.setProperty("REORGANIZATION_TIMEOUT", Integer.toString(DEFAULT_REORGANIZATION_TIMEOUT));
		}
		if (!props.containsKey("HTTP_START")) {
			props.setProperty("HTTP_START", Integer.toString(DEFAULT_HTTP_START));
		}
		if (!props.containsKey("HTTP_PERIOD")) {
			props.setProperty("HTTP_PERIOD", Integer.toString(DEFAULT_HTTP_PERIOD));
		}
		if (!props.containsKey("TRANSFER_START")) {
			props.setProperty("TRANSFER_START", Integer.toString(DEFAULT_TRANSFER_START));
		}		
		if (!props.containsKey("TRANSFER_PERIOD")) {
			props.setProperty("TRANSFER_PERIOD", Integer.toString(DEFAULT_TRANSFER_PERIOD));
		}		
		if (!props.containsKey("TRANSFER_COUNT")) {
			props.setProperty("TRANSFER_COUNT", Integer.toString(DEFAULT_TRANSFER_COUNT));
		}		
		if (!props.containsKey("ACCEPT_TRANSFER")) {
			props.setProperty("ACCEPT_TRANSFER", Boolean.toString(DEFAULT_ACCEPT_TRANSFER));
		}		
		if (!props.containsKey("EXPIRE_TIME")) {
			props.setProperty("EXPIRE_TIME", Long.toString(DEFAULT_EXPIRE_TIME));
		}		
	}
	
	private static synchronized Properties getProperties() {
		if (props == null) {
			props = new Properties();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream("config.ini");
				props.load(fis);
			} catch(FileNotFoundException ex) {
				props = new Properties();
			} catch (Exception e) {
				props = new Properties();
				LOG.log(Level.SEVERE, "Error while loading configuration", e);
			} finally {
				try {
					if (fis != null) fis.close();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, "Error while closing configuration file", e);					
				}
			}
			
			checkProperties(props);

			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream("config.ini");
				props.store(fos, "");
			} catch (Exception e) {
				props = new Properties();
				LOG.log(Level.SEVERE, "Error while loading configuration", e);
			} finally {
				try {
					if (fos != null) fos.close();
				} catch (Exception e) {
					LOG.log(Level.SEVERE, "Error while closing configuration file after save", e);					
				}				
			}
		}
		return props;
	}
	
	
}
