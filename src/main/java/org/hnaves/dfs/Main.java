package org.hnaves.dfs;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

import org.hnaves.dfs.election.Election;
import org.hnaves.dfs.election.Node;
import org.hnaves.dfs.election.socket.UDPSocket;
import org.hnaves.dfs.http.HttpServer;
import org.hnaves.dfs.repository.Repository;

public class Main {
	
	private void run(String[] args) throws SecurityException, IOException {
		InputStream is = this.getClass().getResourceAsStream("/log/log.properties");
		LogManager.getLogManager().readConfiguration(is);
		Node node = new Node();
		System.out.println(node);
		Repository repository = new Repository(node, new File("."));
		UDPSocket socket = new UDPSocket(Configurator.getUdpPort());
		Election election = new Election(node, socket);
		HttpServer server = new HttpServer(election, repository, Configurator.getTcpPort());
		server.run();		
	}
	
	public static void main(String[] args) throws Exception {
		new Main().run(args);
	}
}
