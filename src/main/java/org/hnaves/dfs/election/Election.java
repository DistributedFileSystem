package org.hnaves.dfs.election;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hnaves.dfs.Configurator;
import org.hnaves.dfs.election.messages.Accept;
import org.hnaves.dfs.election.messages.AcceptReply;
import org.hnaves.dfs.election.messages.AreYouCoordinator;
import org.hnaves.dfs.election.messages.AreYouCoordinatorReply;
import org.hnaves.dfs.election.messages.AreYouThere;
import org.hnaves.dfs.election.messages.AreYouThereReply;
import org.hnaves.dfs.election.messages.Invitation;
import org.hnaves.dfs.election.messages.Message;
import org.hnaves.dfs.election.messages.Ready;
import org.hnaves.dfs.election.messages.ReadyReply;
import org.hnaves.dfs.election.socket.Socket;


public class Election {
	private static final Logger LOG = Logger.getLogger(Election.class.getName());
	
	private final Socket socket;
	private final Node id;

	private long group;
	private Node groupCoordinator;
	private Status status;

	private boolean isCoordinatorAlive;
	private long lastDiscriminator;
	private long lastElectionTime;
	private long lastReorganizationTime;
	
	private int numReadyAnswers;
	
	private final Set<Node> upNodes = new HashSet<Node>();
	private final Set<Node> otherCoordinators = new HashSet<Node>();
	
	private final ArrayList<ElectionListener> listeners = new ArrayList<ElectionListener>();
	
	private class ReceiverThread extends Thread {
		public ReceiverThread() { super("Receiver Thread"); }
		@Override
		public void start() {
			setPriority(MIN_PRIORITY);
			super.start();
		}

		@Override
		public void run() {
			while(true) {
				synchronized(Election.this) {
					if (status == Status.DOWN) break;
				}
				try {
					Message message = socket.receiveMessage();
					if (message != null && !message.getFrom().equals(id)) {
						LOG.fine("Processing message " + message + "\n");
						processMessage(message);
					}
				} catch(Throwable t) {
					LOG.log(Level.SEVERE, "Exception while receiving message", t);
				}
			}
		}		
	}
	private final Thread receiverThread = new ReceiverThread();
	

	private class MainThread extends Thread {
		private MainThread() { super("Main Thread"); setPriority(Thread.MIN_PRIORITY); }
		@Override
		public void start() {
			setPriority(MIN_PRIORITY);
			super.start();
		}
		@Override
		public void run() {
			while(true) {
				synchronized(Election.this) {
					if (status == Status.DOWN) break;
				}
				try {
					check();
					merge();
					recovery();
					timeout();
				} catch(Throwable t) {
					LOG.log(Level.SEVERE, "Exception while running", t);
				}
			}			
		}
	}
	private final Thread mainThread = new MainThread();
	
	public Election(Node id, Socket socket) {
		if (id == null) throw new NullPointerException("Id is null");
		if (socket == null) throw new NullPointerException("Socket is null");
		this.id = id;
		this.socket = socket;
	}

	public Node getId() {
		return id;
	}
	
	public synchronized Node getGroupCoordinatorIfNormal() {
		if (status == Status.NORMAL) {
			return groupCoordinator;
		}
		return null;
	}
	
	public synchronized Status getStatus() {
		return status;
	}
	
	public synchronized long getGroup() {
		return group;
	}

	public synchronized Node getGroupCoordinator() {
		return groupCoordinator;
	}

	public synchronized void start() {
		if (status == null) {
			LOG.fine("Starting Node " + id +  "...\n");
			setStatus(Status.RECOVERY);
			receiverThread.start();
			mainThread.start();
		}
	}
	
	public void stop() throws InterruptedException {
		synchronized(this) {
			LOG.fine("Stopping Node " + id + " ...\n");
			setStatus(Status.DOWN);
		}
		mainThread.join();
		receiverThread.join();
	}
	
	public synchronized void registerListener(ElectionListener listener) {
		if (listener == null) throw new NullPointerException("Listener is null");
		listeners.add(listener);
	}

	public synchronized void unregisterListener(ElectionListener listener) {
		if (listener == null) throw new NullPointerException("Listener is null");
		listeners.remove(listener);
	}

	
	private synchronized void processMessage(Message message) throws IOException {
		if (message instanceof AreYouCoordinator) {
			processAreYouCoordinator((AreYouCoordinator) message);
		} else if (message instanceof AreYouCoordinatorReply) {
			processAreYouCoordinatorReply((AreYouCoordinatorReply) message);
		} else if (message instanceof AreYouThere) {
			processAreYouThere((AreYouThere) message);
		} else if (message instanceof AreYouThereReply) {
			processAreYouThereReply((AreYouThereReply) message);
		} else if (message instanceof Invitation) {
			processInvitation((Invitation) message);
		} else if (message instanceof Accept) {
			processAccept((Accept) message);
		} else if (message instanceof AcceptReply) {
			processAcceptReply((AcceptReply) message);
		} else if (message instanceof Ready) {
			processReady((Ready) message);
		} else if (message instanceof ReadyReply) {
			processReadyReply((ReadyReply) message);
		}
	}

	private void processAreYouCoordinator(AreYouCoordinator message) throws IOException {
		AreYouCoordinatorReply reply;
		if (status == Status.NORMAL && isGroupCoordinator()) {
			reply = new AreYouCoordinatorReply(id, message.getFrom(), message.getDiscriminator(), true);
		} else {
			reply = new AreYouCoordinatorReply(id, message.getFrom(), message.getDiscriminator(), false);				
		}
		sendMessage(reply);		
	}
	
	private void processAreYouCoordinatorReply(AreYouCoordinatorReply reply) throws IOException {
		if (reply.getReply() && reply.getDiscriminator() == lastDiscriminator) // To avoid unnecessary merges
			otherCoordinators.add(reply.getFrom());		
	}
	
	private void processAreYouThere(AreYouThere message) throws IOException {
		AreYouThereReply reply;
		if (group == message.getGroup() && isGroupCoordinator()) { // Do not check status, because non-coordinator processors become normal before the coordinator
			reply = new AreYouThereReply(id, message.getFrom(), true);
		} else {
			reply = new AreYouThereReply(id, message.getFrom(), false);
		}
		sendMessage(reply);
	}
	
	private void processAreYouThereReply(AreYouThereReply reply) throws IOException {
		if (!isGroupCoordinator())
			isCoordinatorAlive = reply.getReply();
	}
	
	private void processInvitation(Invitation message) throws IOException {
		if (status == Status.NORMAL) {
			setStatus(Status.ELECTION);
			lastElectionTime = getTime();

			if (isGroupCoordinator()) {
				for(Node nodeId : upNodes) {
					Invitation invitation = new Invitation(id, nodeId, message.getGroupCoordinator(), message.getGroup());
					sendMessage(invitation);
				}
			}
			setGroupAndCoordinator(message.getGroup(), message.getGroupCoordinator());
			Accept accept = new Accept(id, groupCoordinator, group);
			sendMessage(accept);
		}		
	}
	
	private void processReady(Ready message) throws IOException {
		ReadyReply reply; 
		if (status == Status.REORGANIZATION && group == message.getGroup()) {
			setStatus(Status.NORMAL);
			reply = new ReadyReply(id, message.getFrom(), true, group);
		} else {
			reply = new ReadyReply(id, message.getFrom(), false, group);			
		}
		sendMessage(reply);
	}
	
	private void processReadyReply(ReadyReply reply) throws IOException {
		if (reply.getReply() && reply.getGroup() == group) {
			numReadyAnswers++;
		}
	}

	private void processAccept(Accept message) throws IOException {
		AcceptReply reply;
		if (status == Status.ELECTION && isGroupCoordinator() && message.getGroup() == group) {
			upNodes.add(message.getFrom());
			reply = new AcceptReply(id, message.getFrom(), true);
		} else {
			reply = new AcceptReply(id, message.getFrom(), false);			
		}
		sendMessage(reply);
	}

	private void processAcceptReply(AcceptReply reply) throws IOException {
		if (status == Status.ELECTION && groupCoordinator.equals(reply.getFrom())) {
			if (reply.getReply()) {
				setStatus(Status.REORGANIZATION);
				lastReorganizationTime = getTime();
			}
		}
	}
	
	private synchronized void recovery() {
		if (status == Status.ELECTION && checkElectionTimeout())
			setStatus(Status.RECOVERY);
		if (status == Status.REORGANIZATION && checkReorganizationTimeout())
			setStatus(Status.RECOVERY);

		if (status != Status.RECOVERY && status != Status.DOWN) return;
		
		setStatus(Status.ELECTION);
		setGroupAndCoordinator(generateGroup(), id);
		upNodes.clear();
		setStatus(Status.REORGANIZATION);
		setStatus(Status.NORMAL);
	}
	
	private synchronized void check() throws IOException, InterruptedException {
		if (status != Status.NORMAL || !isGroupCoordinator()) return;
		
		otherCoordinators.clear();
		lastDiscriminator = generateDiscriminator();
		AreYouCoordinator message = new AreYouCoordinator(id, Node.BROADCAST_NODE, lastDiscriminator);
		sendMessage(message);
		wait(Configurator.getAreYouCoordinatorTimeout());
		wait((int) (Math.random() * Configurator.getAreYouCoordinatorRandomFactor()));

		if (!otherCoordinators.isEmpty() && isGroupCoordinator()) {
			LOG.fine("Other coordinator of " + id + " " + otherCoordinators + "\n");
			setStatus(Status.MERGE);
		}
	}
	
	private synchronized void merge() throws IOException, InterruptedException {
		if (status != Status.MERGE) return;
		
		setStatus(Status.ELECTION);
		setGroupAndCoordinator(generateGroup(), id);
		Set<Node> nodes = new HashSet<Node>(upNodes);
		upNodes.clear();
		nodes.addAll(otherCoordinators);
		for(Node nodeId : nodes) {
			Invitation invitationMessage = new Invitation(id, nodeId, groupCoordinator, group);
			sendMessage(invitationMessage);
		}
		wait(Configurator.getInvitationTimeout());
		
		setStatus(Status.REORGANIZATION);

		numReadyAnswers = 0;
		for(Node nodeId : upNodes) {
			Ready readyMessage = new Ready(id, nodeId, group);
			sendMessage(readyMessage);
		}

		wait(Configurator.getReadyTimeout());
		if (numReadyAnswers < upNodes.size()) {
			LOG.fine("Missing " + (upNodes.size() - numReadyAnswers) + " ready answers in " + id + "\n");
			setStatus(Status.RECOVERY);
		} else {
			setStatus(Status.NORMAL);
		}
	}
	
	private synchronized void timeout() throws IOException, InterruptedException {
		if (status != Status.NORMAL || id.equals(groupCoordinator)) return;
		
		AreYouThere message = new AreYouThere(id, groupCoordinator, group);
		isCoordinatorAlive = false;
		sendMessage(message);
		wait(Configurator.getAreYouThereTimeout());

		if (!isCoordinatorAlive) {
			LOG.fine("Coordinator is not alive in node "  + id + "!!!\n");
			setStatus(Status.RECOVERY);
		}
	}
	
	private boolean checkElectionTimeout() {
		return (lastElectionTime + Configurator.getElectionTimeout() < getTime());
	}
	
	private boolean checkReorganizationTimeout() {
		return (lastReorganizationTime + Configurator.getReorganizationTimeout() < getTime());
	}
	
	private long getTime() {
		return System.currentTimeMillis();
	}
	
	private long generateDiscriminator() {
		return getTime();
	}
	
	private long generateGroup() {
		return generateDiscriminator();
	}
	
	private void setGroupAndCoordinator(long group, Node groupCoordinator) {
		LOG.fine("Changing to group " + group  + " coordinator " + groupCoordinator + " in node " + id + "\n");
		this.group = group;
		this.groupCoordinator = groupCoordinator;
	}

	private boolean isGroupCoordinator() {
		return id.equals(groupCoordinator);
	}

	private void setStatus(Status status) {
		LOG.fine("Entering " + status  + " status in node " + id + "\n");
		this.status = status;
		for(ElectionListener listener : listeners) {
			listener.onStateChange(this);
		}
		
	}
	
	private void sendMessage(Message message) throws IOException {
		LOG.fine("Sending message " + message + "\n");
		socket.sendMessage(message);
	}
	
	
	
}
