package org.hnaves.dfs.election;

public interface ElectionListener {
	public void onStateChange(Election election);
}
