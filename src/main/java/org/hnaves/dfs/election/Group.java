package org.hnaves.dfs.election;

import java.io.Serializable;


public class Group implements Serializable {
	private static final long serialVersionUID = 1912100815705396637L;

	private final Node coordinator;
	private final int groupNumber;

	public Group(Node coordinator, int groupNumber) {
		if (coordinator == null) throw new NullPointerException("Coordinator is null");
		this.coordinator = coordinator;
		this.groupNumber = groupNumber;
	}
	
	public Node getCoordinator() {
		return coordinator;
	}

	public int getGroupNumber() {
		return groupNumber;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (!(obj instanceof Group)) return false;
		Group other = (Group) obj;
		return getGroupNumber() == other.getGroupNumber() &&
			getCoordinator().equals(other.getCoordinator());
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 17;
		int hash = getGroupNumber();
		hash = hash * PRIME + getCoordinator().hashCode();
		return hash;
	}

	@Override
	public String toString() {
		return "Group " + getGroupNumber() + "[" + getCoordinator() + "]";
	}
}
