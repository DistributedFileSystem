package org.hnaves.dfs.election;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;


public class Node implements Serializable {
	private static final long serialVersionUID = 6339718337514260251L;

	private final String name;
	private final InetAddress address;
	
	public final static Node BROADCAST_NODE;
	
	static {
		 try {
			BROADCAST_NODE = new Node("Broadcast", InetAddress.getByName("255.255.255.255"));
		} catch (UnknownHostException e) {
			throw new RuntimeException(e);
		}
	}
	
	public Node() {
		this(null);
	}

	public Node(String name) {
		this(name, getLocalHost());
	}

	public Node(String name, InetAddress address) {
		if (address == null)
			throw new NullPointerException("Address is null");
		if (name == null)
			name = getDefaultName();
		this.name = name;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public InetAddress getAddress() {
		return address;
	}

	private static InetAddress getLocalHost() {
		try {
			InetAddress address = InetAddress.getLocalHost();
			if (address.isLoopbackAddress()) {
				Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();

				search:
				while (e.hasMoreElements()) {
					NetworkInterface ni = e.nextElement();
					if (ni.isLoopback()) continue;
					if (!ni.isUp()) continue;

					Enumeration<InetAddress> e2 = ni.getInetAddresses();

					while (e2.hasMoreElements()) {
						InetAddress ip = (InetAddress) e2.nextElement();
						if (ip instanceof Inet4Address) {
							address = ip;
							break search;
						}
					}
				}
			}
			return address;			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private static String getDefaultName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Node))
			return false;
		Node other = (Node) obj;
		return this.getAddress().equals(other.getAddress());
	}

	@Override
	public int hashCode() {
		return getAddress().hashCode();
	}

	@Override
	public String toString() {
		return getName() + "@" + getAddress().toString();
	}
}
