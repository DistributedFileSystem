package org.hnaves.dfs.election;

public enum Status {
	DELAY,
	TIMEOUT,
	NORMAL,
	DOWN,
	ELECTION,
	REORGANIZATION,
	RECOVERY,
	MERGE;
}
