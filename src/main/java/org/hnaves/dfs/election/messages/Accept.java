package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class Accept extends Message {
	private static final long serialVersionUID = -7378319451783314617L;
	private final long group;
	
	public Accept(Node from, Node to, long group) {
		super(from, to);
		this.group = group;
	}
	
	public long getGroup() {
		return group;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		Accept other = (Accept) obj;
		return getGroup() == other.getGroup();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 7;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getGroup(); 
		return hash;
	}
	
	
	@Override
	public String toString() {
		return "Accept[From=" + getFrom() + ";To=" + getTo() + ";Group=" + getGroup() + "]";
	}
}
