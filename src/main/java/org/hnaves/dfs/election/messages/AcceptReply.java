package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class AcceptReply extends Reply {
	private static final long serialVersionUID = 8939039254919930081L;
	
	public AcceptReply(Node from, Node to, boolean reply) {
		super(from, to, reply);
	}

	@Override
	public String toString() {
		return "AcceptReply[From=" + getFrom() + ";To=" + getTo() + ";Reply=" + getReply() + "]";
	}
	
}
