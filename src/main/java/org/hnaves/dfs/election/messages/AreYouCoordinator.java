package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class AreYouCoordinator extends Message {

	private static final long serialVersionUID = -8680899728650212010L;
	
	private final long discriminator;
	
	public AreYouCoordinator(Node from, Node to, long discriminator) {
		super(from, to);
		this.discriminator = discriminator;
	}
	
	public long getDiscriminator() {
		return discriminator;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		AreYouCoordinator other = (AreYouCoordinator) obj;
		return getDiscriminator() == other.getDiscriminator();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 5;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getDiscriminator();
		return hash;
	}
	
	
	@Override
	public String toString() {
		return "AreYouCoordinator[From=" + getFrom() + ";To=" + getTo() + ";Discriminator=" + getDiscriminator() + "]";
	}	

}
