package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class AreYouCoordinatorReply extends Reply {
	private static final long serialVersionUID = 2179618896540707515L;
	
	private final long discriminator;
	
	public AreYouCoordinatorReply(Node from, Node to, long discriminator, boolean reply) {
		super(from, to, reply);
		this.discriminator = discriminator;
	}
	
	public long getDiscriminator() {
		return discriminator;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		AreYouCoordinatorReply other = (AreYouCoordinatorReply) obj;
		return getDiscriminator() == other.getDiscriminator();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 5;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getDiscriminator();
		return hash;
	}
	
		

	@Override
	public String toString() {
		return "AreYouCoordinatorReply[From=" + getFrom() + ";To=" + getTo() + ";Reply=" + getReply() + ";Discriminator=" + getDiscriminator() + "]";
	}
}
