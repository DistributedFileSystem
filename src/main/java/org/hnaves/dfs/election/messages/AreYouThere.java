package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class AreYouThere extends Message {
	private static final long serialVersionUID = 2956915022868414149L;
	private final long group;
	
	public AreYouThere(Node from, Node to, long group) {
		super(from, to);
		this.group = group;
	}
	
	public long getGroup() {
		return group;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		AreYouThere other = (AreYouThere) obj;
		return getGroup() == other.getGroup();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getGroup(); 
		return hash;
	}
	
	
	@Override
	public String toString() {
		return "AreYouThere[From=" + getFrom() + ";To=" + getTo() + ";Group=" + getGroup() + "]";
	}	
}
