package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class AreYouThereReply extends Reply {

	private static final long serialVersionUID = -4572323521319620743L;

	public AreYouThereReply(Node from, Node to, boolean reply) {
		super(from, to, reply);
	}

	@Override
	public String toString() {
		return "AreYouThereReply[From=" + getFrom() + ";To=" + getTo() + ";Reply=" + getReply() + "]";
	}
}
