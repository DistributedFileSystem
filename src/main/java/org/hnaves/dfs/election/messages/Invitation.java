package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class Invitation extends Message {

	private static final long serialVersionUID = 4212839018116812947L;
	
	private final long group;
	private final Node groupCoordinator;

	public Invitation(Node from, Node to, Node groupCoordinator, long group) {
		super(from, to);
		if (groupCoordinator == null) throw new NullPointerException("Group coordinator is null");
		this.groupCoordinator = groupCoordinator;
		this.group = group;
	}

	public Node getGroupCoordinator() {
		return groupCoordinator;
	}

	public long getGroup() {
		return group;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		Invitation other = (Invitation) obj;
		return getGroup() == other.getGroup() &&
			getGroupCoordinator().equals(other.getGroupCoordinator());
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getGroup();
		hash = hash * PRIME + getGroupCoordinator().hashCode();
		return hash;
	}
	
	
	@Override
	public String toString() {
		return "Invitation[From=" + getFrom() + ";To=" + getTo() + ";Coordinator=" + getGroupCoordinator() + ";Group=" + getGroup() + "]";
	}
	
}
