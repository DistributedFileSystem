package org.hnaves.dfs.election.messages;

import java.io.Serializable;

import org.hnaves.dfs.election.Node;

public abstract class Message implements Serializable {

	private static final long serialVersionUID = -5833948973846065999L;
	private final Node from;
	private final Node to;
	
	public Message(Node from, Node to) {
		if (from == null) throw new NullPointerException("From is null");
		if (to == null) throw new NullPointerException("To is null");
		this.from = from;
		this.to = to;
	}
	
	public Node getFrom() {
		return from;
	}
	
	public Node getTo() {
		return to;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (!(obj instanceof Message)) return false;
		if (obj.getClass() != this.getClass()) return false;
		Message other = (Message) obj;
		return getFrom().equals(other.getFrom()) &&
			getTo().equals(other.getTo());
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 41;
		int hash = getFrom().hashCode();
		hash = hash * PRIME + getTo().hashCode();
		return hash;
	}		
}
