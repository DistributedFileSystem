package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class Ready extends Message {
	private static final long serialVersionUID = -3296713936502096262L;
	private final long group;
	

	public Ready(Node from, Node to, long group) {
		super(from, to);
		this.group = group;
	}


	public long getGroup() {
		return group;
	}


	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		Ready other = (Ready) obj;
		return getGroup() == other.getGroup();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 17;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getGroup();
		return hash;
	}
	
	
	@Override
	public String toString() {
		return "Ready[From=" + getFrom() + ";To=" + getTo() + ";Group=" +  getGroup() + "]";
	}
	

}
