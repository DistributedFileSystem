package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public class ReadyReply extends Reply {
	private static final long serialVersionUID = -7018048199898783170L;
	private final long group;
	
	public ReadyReply(Node from, Node to, boolean reply, long group) {
		super(from, to, reply);
		this.group = group;
	}
	
	public long getGroup() {
		return group;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		ReadyReply other = (ReadyReply) obj;
		return getGroup() == other.getGroup();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 17;
		int hash = super.hashCode();
		hash = hash * PRIME + (int) getGroup();
		return hash;
	}
	
	
	@Override
	public String toString() {
		return "ReadyReply[From=" + getFrom() + ";To=" + getTo() + ";Reply=" + getReply() + ";Group=" + getGroup() + "]";
	}

}
