package org.hnaves.dfs.election.messages;

import org.hnaves.dfs.election.Node;

public abstract class Reply extends Message {
	private static final long serialVersionUID = -7252871074628846221L;
	private final boolean reply;
	
	public Reply(Node from, Node to, boolean reply) {
		super(from, to);
		this.reply = reply;
	}
	
	public boolean getReply() {
		return reply;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) return false;
		Reply other = (Reply) obj;
		return getReply() == other.getReply();
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 37;
		int hash = super.hashCode();
		hash = hash * PRIME + (getReply() ? 1 : 0);
		return hash;
	}	
}
