package org.hnaves.dfs.election.socket;

import java.io.IOException;

import org.hnaves.dfs.election.messages.Message;

public interface Socket {

	public void sendMessage(Message message) throws IOException;
	public Message receiveMessage() throws IOException;
	
}
