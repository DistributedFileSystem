package org.hnaves.dfs.election.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hnaves.dfs.Configurator;
import org.hnaves.dfs.election.messages.Message;
import org.hnaves.dfs.repository.utils.ObjectUtils;

public class UDPSocket implements Socket {
	private static final Logger LOG = Logger.getLogger(UDPSocket.class.getName());
	private final DatagramSocket socket;
	private final int port;
	
	public UDPSocket() {
		this(Configurator.getUdpPort());
	}

	public UDPSocket(int port) {
		try {
			this.port = port;
			socket = new DatagramSocket(port);
			socket.setBroadcast(true);
			socket.setSoTimeout(500);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	private DatagramPacket createPacket(Message message) throws IOException {
		byte[] data = ObjectUtils.serializeObject(message);
		LOG.fine("Packet size " + data.length);
		DatagramPacket packet = new DatagramPacket(data, data.length, message.getTo().getAddress(), port);
		return packet;
	}
	
	public void sendMessage(Message message) throws IOException {
		if (message == null) throw new NullPointerException("Message is null");
		LOG.fine("Send Message " + message + " to " + message.getTo());
		DatagramPacket packet = createPacket(message);
		socket.send(packet);
	}
	

	public Message receiveMessage() {
		DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
		try {
			socket.receive(packet);
			LOG.fine("Receiving from " + packet.getSocketAddress());
			Message message = (Message) ObjectUtils.deserializeObject(packet.getData());
			LOG.fine("Received Message " + message);
			return message;
		} catch(SocketTimeoutException ex) {
			return null;
		} catch (Throwable t) {
			LOG.log(Level.SEVERE, "Exception while receiveMessage", t);
			return null;
		}
	}
}
