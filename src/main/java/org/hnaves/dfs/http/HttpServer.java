package org.hnaves.dfs.http;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hnaves.dfs.Configurator;
import org.hnaves.dfs.election.Election;
import org.hnaves.dfs.election.ElectionListener;
import org.hnaves.dfs.election.Node;
import org.hnaves.dfs.election.Status;
import org.hnaves.dfs.repository.Container;
import org.hnaves.dfs.repository.ContentItem;
import org.hnaves.dfs.repository.CopyOperation;
import org.hnaves.dfs.repository.Repository;
import org.hnaves.dfs.repository.utils.ObjectUtils;

public class HttpServer implements ElectionListener {
	private static final Logger LOG = Logger.getLogger(HttpServer.class.getName());
	private final static String CRLF = "\r\n";

	private final static DateFormat dateFormat = DateFormat.getInstance();
	private final Election election;
	private final Repository repository;
	private final int port;

	public HttpServer(Election election, Repository repository, int port) {
		if (election == null) throw new NullPointerException("Election is null");
		if (repository == null) throw new NullPointerException("Repository is null");
		this.repository = repository;
		this.election = election;
		this.port = port;
	}

	public void onStateChange(Election election) {
		if (election.getStatus() != Status.NORMAL) {
			repository.clearRemoteFiles();
		}
	}


	public void run() {
		repository.loadIndex();

		election.registerListener(this);
		election.start();
		Timer updateTimer = new Timer("Update Timer");
		updateTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				repository.updateLocalFiles();
				
				try {
					Node coordinator = election.getGroupCoordinatorIfNormal();
					if (coordinator != null && !coordinator.equals(election.getId())) {
						LOG.fine("Updating to server " + coordinator);
						ByteArrayInputStream bais =
							new ByteArrayInputStream(ObjectUtils.serializeObject(
									repository.getLocalFiles()));
						makeRequest(coordinator.getAddress(), port,
								"POST /update HTTP/1.1\r\n\r\n", bais, null);
					}
				} catch(Exception ex) {
					LOG.log(Level.SEVERE,"Error in update timer task", ex);
				}
			}
			
		}, Configurator.getHttpStart(), Configurator.getHttpPeriod());
		
		Timer transferTimer = new Timer("Transfer Timer");
		transferTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					Node coordinator = election.getGroupCoordinatorIfNormal();
					if (election.getId().equals(coordinator)) {
						CopyOperation op = repository.randomCopyOperation(Configurator.getTransferCount());
						if (op != null) {
							LOG.fine("Notification about copy to " + op.getDestination());
							if (op.getDestination().equals(election.getId())) {
								new Thread(new HttpCopy(op)).start();
							} else {
								ByteArrayInputStream bais = new ByteArrayInputStream(ObjectUtils.serializeObject(op));
								makeRequest(op.getDestination().getAddress(), port,
										"POST /transfer HTTP/1.1\r\n\r\n", bais, null);
							}
						}
					}
				} catch(Exception ex) {
					LOG.log(Level.SEVERE,"Error in transfer task", ex);
				}
			}
			
		}, Configurator.getTransferStart(), Configurator.getTransferPeriod());

		ServerSocket serverSocket;
		try {

			serverSocket = new ServerSocket(port);

			while (true) {
				try {
					Socket socket = serverSocket.accept();
					LOG.info("New connection accepted "
							+ socket.getInetAddress() + ":" + socket.getPort());

					HttpRequestHandler request = new HttpRequestHandler(socket);
					Thread thread = new Thread(request);
					thread.start();
				} catch (Exception e) {
					LOG.log(Level.SEVERE,"Error while handling request", e);
				}
			}
		} catch (IOException e) {
			LOG.log(Level.SEVERE,"IO error", e);
		}

	}

	private class HttpRequestHandler implements Runnable {

		private final Socket socket;
		private final OutputStream output;
		private final InputStream is;

		// Constructor
		public HttpRequestHandler(Socket socket) throws Exception {
			this.socket = socket;
			this.output = socket.getOutputStream();
			this.is = socket.getInputStream();
		}

		// Implement the run() method of the Runnable interface.
		public void run() {
			try {
				processRequest();
			} catch (Exception e) {
				LOG.log(Level.SEVERE,"Exception in request handler", e);
			} finally {
				try {output.close();} catch(Exception ex) {
					LOG.log(Level.SEVERE,"Exception while closing output stream", ex);
				}
				try{is.close();} catch(Exception ex) {
					LOG.log(Level.SEVERE,"Exception while closing input stream", ex);
				}
				try{socket.close();} catch(Exception ex) {
					LOG.log(Level.SEVERE,"Exception while closing socket", ex);
				}
			}
		}

		private void processRequest() throws Exception {
			String postRequest = null;
			while (true) {
				String headerLine = readLine(is);
				if (headerLine == null || CRLF.equals(headerLine) || "".equals(headerLine) ||
						"\n".equals(headerLine))
					break;

				StringTokenizer tokenizer = new StringTokenizer(headerLine);
				String cmd = tokenizer.nextToken();

				if ("GET".equals(cmd)) {
					String request = tokenizer.nextToken();
					LOG.fine("Request: " + request);
					
					if ("/local/".equals(request)) {
						sendListing(true);
					} else {
						if ("/".equals(request)) {
							Node coordinator = election.getGroupCoordinatorIfNormal();
							
							if (coordinator == null) {
								sendListing(true);
							} else if (!election.getId().equals(coordinator)) {
								sendRedirect(request, coordinator);
							} else {
								sendListing(false);
							}
						} else {
							sendFile(request);
						}
					}
				} else if ("POST".equals(cmd)) {
					postRequest = tokenizer.nextToken();
				}
			}
			
			if (postRequest != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				transferBytes(is, baos);
				
				if ("/update".equals(postRequest)) {
					Container c = (Container) ObjectUtils.deserializeObject(baos.toByteArray());
					repository.updateRemoteFiles(c);
					output.write(("OK\r\n\r\n").getBytes());
				} else if ("/transfer".equals(postRequest)) {
					if (Configurator.getAcceptTransfer()) {
						CopyOperation op = (CopyOperation) ObjectUtils.deserializeObject(baos.toByteArray());
						new Thread(new HttpCopy(op)).start();
						output.write(("OK\r\n\r\n").getBytes());
					} else {
						output.write(("ERROR\r\n\r\n").getBytes());
					}
				}
			}
		}
		
		private void appendListing(StringBuilder sb, Container c) {
			sb.append("<tr><td colspan=\"4\"><h1>Machine ").append(c.getLocation()).append("</h1></td></tr>");
			sb.append("<tr><th>Name</th><th>Last modified</th><th>size</th><th>hash</th></tr>\r\n");
			sb.append("<tr><th colspan=\"4\"><hr /></th></tr>\r\n");
			for (ContentItem item : c.getContent()) {
				sb.append("<tr><td>");
				sb.append("<a href=\"http://").append(
						c.getLocation().getAddress().getHostAddress()).append(":").append(port)
						.append("/").append(item.getName()).append("\">")
						.append(item.getName()).append("</a><br />");
				sb.append("</td><td>").append(dateFormat.format(item.getLastModified()))
				.append("</td><td><i>");
				sb.append(item.getSize()).append(" bytes</i></td><td><b>").append(
						item.getMd5Hash()).append("</b></td></tr>\r\n");
			}
			sb.append("<tr><th colspan=\"4\"><hr /></th></tr>\r\n");
		}
		
		private void sendListing(boolean local) throws IOException {
			String statusLine = "HTTP/1.0 200 OK\r\n";
			String contentTypeLine = "Content-type: text/html\r\n";
			StringBuilder sb = new StringBuilder();
			sb.append("<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n");
			sb.append("<head><title>Directory Listing</head><body>\r\n");
			sb.append("<h1>Status</h1><ul><li>State:").append(election.getStatus()).append("</li>");
			sb.append("<li>Group: ").append(election.getGroup()).append("</li>");
			sb.append("<li>Group Coordinator: ").append(election.getGroupCoordinator()).append("</li>");
			sb.append("</ul>");
			sb.append("<table>");
			if (!local) {
				List<Container> allFiles = null;
				allFiles = repository.getAllFiles();
				for (Container c : allFiles) {
					appendListing(sb, c);
				}
			} else {
				appendListing(sb, repository.getLocalFiles());
			}
			sb.append("</table></body></html>\r\n");
			String contentLengthLine = "Content-Length: " + sb.length() + "\r\n\r\n";

			output.write(statusLine.getBytes());
			output.write(contentTypeLine.getBytes());
			output.write(contentLengthLine.getBytes());
			output.write(sb.toString().getBytes());
		}

		private void sendRedirect(String request, Node location)
				throws IOException {
			String response = "HTTP/1.0 302 Found\r\n" +
				"Location: http://" + location.getAddress().getHostAddress() + ":"
					+ port + request + "\r\n\r\n";

			output.write(response.getBytes());
		}

		private void sendNotFound(String request) throws IOException {
			String entityBody = "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n"
				+ "<HTML><HEAD>\r\n"
				+ "<TITLE>404 Not Found</TITLE>\r\n"
				+ "</HEAD><BODY>\r\n"
				+ "<H1>Not Found</H1>\r\n"
				+ "The requested URL " + request
				+ " was not found on this server.<P>\r\n" + "</BODY></HTML>\r\n";

			String response = "HTTP/1.0 404 Not Found\r\n" +
				"Content-type: text/html\r\n" +
				"Content-Length: " + entityBody.length() + "\r\n\r\n";

			output.write(response.getBytes());
			output.write(entityBody.getBytes());
		}

		private void sendFile(String request) throws IOException {
			FileInputStream fis = null;
			String fileName = "." + request;
			try {
				File file = new File(repository.getRepositoryDirectory(), fileName);
				fis = new FileInputStream(file);

				String response = "HTTP/1.0 200 OK\r\n" +
					"Content-type: " + contentType(request) + "\r\n" +
					"Content-Length: " + fis.available() + "\r\n\r\n";

				output.write(response.getBytes());
				transferBytes(fis, output);
			} catch (FileNotFoundException e) {
				sendNotFound(request);
				return;
			} finally {
				if (fis != null) fis.close();
			}
			
		}

	}
	
	private class HttpCopy implements Runnable {

		private final CopyOperation op;
		
		private HttpCopy(CopyOperation op) {
			this.op = op;
		}
		
		public void run() {
			LOG.fine("Copy: " + op);
			try {
				FileOutputStream fos = new FileOutputStream(".temp");
				makeRequest(op.getSource().getAddress(), port,
						"GET /" + op.getItem().getName() + " HTTP/1.1\r\n\r\n",
						null, fos);

				File temp = new File(".temp");
				ContentItem item = ContentItem.makeContentItem(temp);
				if (op.getItem().getMd5Hash().equals(item.getMd5Hash())) {
					String name = op.getItem().getName();
					while(true) {
						File file = new File(name);
						if (!file.exists()) break;
						name = "_" + name;
					}
				
					temp.renameTo(new File(name));
				} else {
					LOG.log(Level.SEVERE, "MD5 not match " + item.getMd5Hash() + op.getItem().getMd5Hash());
				}
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Exception while copying", e);
			}
		}
		
	}

	private static String readLine(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();
		while(true) {
			int c = is.read();
			if (c == -1) break;
			sb.append((char) c);
			if (c == '\n') {
				break;
			}
		}
		return sb.toString();
	}


	private static String makeRequest(InetAddress address, int port, String header, InputStream is, OutputStream os) {
		Socket clientSocket = null;
		InputStream sis = null;
		OutputStream sos = null;
		try {
			LOG.fine("Making request to " + address + ":" + port);
			clientSocket = new Socket(address, port);

			sis = clientSocket.getInputStream();
			sos = clientSocket.getOutputStream();
			LOG.fine("Sending... " + header);
			sos.write(header.getBytes());
		
			if (is != null)
				transferBytes(is, sos);
			sos.flush();
			clientSocket.shutdownOutput();
			
			StringBuilder out = new StringBuilder();
			
			while (true) {
				String line = readLine(sis);
				if (line == null || "".equals(line) || CRLF.equals(line) || "\n".equals(line))
					break;
				out.append(line);
			}
			
			if (os != null) {
				transferBytes(sis, os);
				os.flush();
			}
			LOG.fine("Received: " + out);
			return out.toString();
		} catch(Exception ex) {
			LOG.log(Level.SEVERE, "Exception during request", ex);
			return null;
		} finally {
			try {
				if (is != null) is.close();
			} catch(Exception ex) {
				LOG.log(Level.SEVERE, "Exception while closing intput stream", ex);				
			}
			try {
				if (os != null) os.close();
			} catch(Exception ex) {
				LOG.log(Level.SEVERE, "Exception while closing output stream", ex);				
			}
			try {
				if (sis != null) sis.close();
			} catch(Exception ex) {
				LOG.log(Level.SEVERE, "Exception while closing socket input stream", ex);				
			}
			try {
				if (sos != null) sos.close();
			} catch(Exception ex) {
				LOG.log(Level.SEVERE, "Exception while closing socket output stream", ex);				
			}
			try {
				if (clientSocket != null) clientSocket.close();
			} catch(Exception ex) {
				LOG.log(Level.SEVERE, "Exception while closing socket", ex);				
			}
		}
	}
	
	private static void transferBytes(InputStream is, OutputStream os)
			throws IOException {
		byte[] buffer = new byte[1024];
		int bytes = 0;

		while ((bytes = is.read(buffer)) != -1) {
			os.write(buffer, 0, bytes);
		}
	}

	private static String contentType(String fileName) {
		if (fileName.endsWith(".htm") || fileName.endsWith(".html")) {
			return "text/html";
		}
		if (fileName.endsWith(".log") || fileName.endsWith(".txt")) {
			return "text/plain";
		}
		return "binary/octet-stream";
	}

}
