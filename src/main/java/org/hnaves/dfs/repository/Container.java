package org.hnaves.dfs.repository;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import org.hnaves.dfs.Configurator;
import org.hnaves.dfs.election.Node;

public class Container implements Serializable {
	private static final long serialVersionUID = 4190763929963328455L;
	private static final Logger LOG = Logger.getLogger(Container.class.getName());
	
	private final Node location;
	private final ArrayList<ContentItem> content = new ArrayList<ContentItem>();
	private transient long lastUpdate;
	
	public Container(Node location) {
		if (location == null) throw new NullPointerException("Location is null");
		this.location = location;
	}
	
	public Node getLocation() {
		return location;
	}
	
	public ContentItem[] getContent() {
		return content.toArray(new ContentItem[0]);
	}
	
	public int getSize() {
		return content.size();
	}
	
	public void clearContent() {
		content.clear();
	}
	
	public void addItem(ContentItem item) {
		if (item == null) throw new NullPointerException("Item is null");
		content.add(item);
	}
	
	public boolean containsItem(ContentItem item) {
		return content.contains(item);
	}
	
	public void addAllItems(Container c) {
		if (c == null) throw new NullPointerException("Container is null");
		content.addAll(c.content);
	}

	public void update() {
		this.lastUpdate = System.currentTimeMillis();
	}
	
	public boolean hasExpired() {
		return (System.currentTimeMillis() > lastUpdate + Configurator.getExpireTime());
	}
	
	public void updateFiles(File directory) {
		if (directory == null) throw new NullPointerException("Directory is null");
		if (!directory.isDirectory()) throw new IllegalArgumentException("Invalid directory " + directory);
		
		HashMap<String, ContentItem> oldContent = new HashMap<String, ContentItem>();
		for(ContentItem item : content) {
			oldContent.put(item.getName(), item);
		}
		
		content.clear();
		for(File file : directory.listFiles()) {
			if (!file.isFile()) continue;
			if (file.getName().startsWith(".")) continue;
			if (file.getName().startsWith("dfs.log")) continue;
			if (file.getName().equals("index.dat") || file.getName().equals("config.ini")) continue;
			
			LOG.fine("Found file " + file.getName());
			ContentItem oldItem = oldContent.get(file.getName());
			if (oldItem != null && oldItem.getLastModified().getTime() == file.lastModified() &&
					(oldItem.getSize() == (int) file.length())) {
				LOG.fine("Using old hash " + oldItem.getMd5Hash());
				content.add(oldItem);
			} else {
				LOG.fine("New item!");
				ContentItem item = ContentItem.makeContentItem(file);
				content.add(item);
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (!(obj instanceof Container)) return false;
		Container other = (Container) obj;
		return location.equals(other.location);
	}
	
	@Override
	public int hashCode() {
		return location.hashCode();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Location: ").append(location).append("\n");
		for(ContentItem item : content) {
			sb.append(item).append('\n');
		}
		return sb.toString();
	}
}
