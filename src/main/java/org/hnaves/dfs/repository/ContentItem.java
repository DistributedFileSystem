package org.hnaves.dfs.repository;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Logger;

public class ContentItem implements Serializable {
	
	private static final long serialVersionUID = -2943849225159883158L;
	private final static Logger LOG = Logger.getLogger(ContentItem.class.getName());

	private final String name;
	private final Date lastModified;
	private final String md5Hash;
	private final int size;
	
	public ContentItem(String name, Date lastModified, String md5Hash, int size) {
		if (name == null) throw new NullPointerException("Name is null");
		if (lastModified == null) throw new NullPointerException("LastModified is null");
		if (md5Hash == null) throw new NullPointerException("MD5Hash is null");
	    this.name = name;
	    this.lastModified = lastModified;
	    this.size = size;
		this.md5Hash = md5Hash;
	}
	
	public static ContentItem makeContentItem(File localFile) {
		if (localFile == null) throw new NullPointerException("Local file is null");
		if (!localFile.exists() || !localFile.isFile() || !localFile.canRead())
			throw new IllegalArgumentException("Local file is invalid");
	    String name = localFile.getName();
	    Date lastModified = new Date(localFile.lastModified());
	    int size = (int) localFile.length();
		try {
			String hash = md5Hash(localFile);
			return new ContentItem(name, lastModified, hash, size);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	private static String md5Hash(File localFile) throws NoSuchAlgorithmException, IOException {
		LOG.fine("Hashing file " + localFile);
		MessageDigest digest = MessageDigest.getInstance("MD5");
		RandomAccessFile f = null;
		try {
			f = new RandomAccessFile(localFile, "r");
			FileChannel fcl = f.getChannel();
			MappedByteBuffer bl = fcl.map(FileChannel.MapMode.READ_ONLY, 0, (int) fcl.size());
			digest.update(bl);
		} finally {
			if (f != null) f.close();
		}
	    byte[] hash = digest.digest();
	    return new BigInteger(1, hash).toString(16);
	}
	
	public String getName() {
		return name;
	}
	
	public Date getLastModified() {
		return lastModified;
	}
	
	public String getMd5Hash() {
		return md5Hash;
	}
	
	public int getSize() {
		return size;
	}
	
	public String getSignature() {
		return getMd5Hash() + ":" + getSize();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (! (obj instanceof ContentItem)) return false;
		ContentItem other = (ContentItem) obj;
		return (other.getMd5Hash().equals(this.getMd5Hash())) &&
			(other.getSize() == this.getSize());
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int hash = getMd5Hash().hashCode();
		hash = hash * PRIME + getSize();
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getName()).append("\tsize:").append(getSize()).append(" bytes\tmodified: ").append(getLastModified()).append("\t\thash:").append(getMd5Hash());
		return sb.toString();
	}
	
}
