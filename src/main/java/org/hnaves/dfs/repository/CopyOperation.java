package org.hnaves.dfs.repository;

import java.io.Serializable;

import org.hnaves.dfs.election.Node;

public class CopyOperation implements Serializable {
	private static final long serialVersionUID = 8665219034062954068L;
	private final Node source;
	private final Node destination;
	private final ContentItem item;
	
	public CopyOperation(Node source, Node destination, ContentItem item) {
		if (source == null) throw new NullPointerException("Source is null");
		if (destination == null) throw new NullPointerException("Destination is null");
		if (item == null) throw new NullPointerException("Item is null");
		this.source = source;
		this.destination = destination;
		this.item = item;
	}

	public Node getSource() {
		return source;
	}

	public Node getDestination() {
		return destination;
	}

	public ContentItem getItem() {
		return item;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (! (obj instanceof CopyOperation)) return false;
		CopyOperation other = (CopyOperation) obj;
		return (other.getSource().equals(this.getSource())) &&
			(other.getDestination().equals(this.getDestination())) &&
			(other.getItem().equals(this.getItem()));
	}
	
	@Override
	public int hashCode() {
		final int PRIME = 31;
		int hash = getSource().hashCode();
		hash = hash * PRIME + getDestination().hashCode();
		hash = hash * PRIME + getItem().hashCode();
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Copy[").append(getSource()).append("->").append(getDestination()).append(":").append(item).append("]");
		return sb.toString();
	}

}
