package org.hnaves.dfs.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hnaves.dfs.election.Node;

public class Repository {
	private static final java.util.logging.Logger LOG = Logger.getLogger(Repository.class.getName());
	
	private final Container localFiles;
	private final ArrayList<Container> remoteFiles = new ArrayList<Container>();
	private final Node location;
	private final File repositoryDirectory;
	
	public Repository(Node location, File repositoryDirectory) {
		if (repositoryDirectory == null) throw new NullPointerException("Local cache directory is null");
		if (location == null) throw new NullPointerException("Location is null");
		this.location = location;
		this.repositoryDirectory = repositoryDirectory;
		
		localFiles = new Container(location);
	}
	
	
	public File getRepositoryDirectory() {
		return repositoryDirectory;
	}
	
	public Node getLocation() {
		return location;
	}
	
	public synchronized Container getLocalFiles() {
		return localFiles;
	}
	

	public synchronized List<Container> getAllFiles() {
		ArrayList<Container> result = new ArrayList<Container>();
		for(Container c : remoteFiles) {
			if (c.hasExpired()) continue;
			result.add(c);
		}
		result.add(localFiles);
		return result;
	}

	public synchronized void clearLocalFiles() {
		localFiles.clearContent();
	}
	
	public synchronized void clearRemoteFiles() {
		remoteFiles.clear();
	}
	
	public synchronized void updateLocalFiles() {
		LOG.fine("Updating local files...");
		localFiles.updateFiles(repositoryDirectory);
		saveIndex();
	}
	
	public synchronized void updateRemoteFiles(Container container) {
		if (container == null) throw new NullPointerException("Container is null");

		container.update();
		int index = remoteFiles.indexOf(container);
		if (index == -1) {
			remoteFiles.add(container);
		} else {
			remoteFiles.set(index, container);
		}
	}
	
	public synchronized void loadIndex() {
		LOG.fine("Loading index...");
		ObjectInputStream ois = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("index.dat");
			ois = new ObjectInputStream(fis);
			Container c = (Container) ois.readObject();
			localFiles.clearContent();
			localFiles.addAllItems(c);
			ois.close();
		} catch (FileNotFoundException e) {
			LOG.fine("Index file not found");
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Exception while reading index", e);
		} finally {
			try {
				if (ois != null) {
					ois.close();
				} else if (fis != null) {
					fis.close();
				}
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Exception while closing index", e);				
			}
		}
	}
	
	public synchronized void saveIndex() {
		LOG.fine("Saving index...");
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream("index.dat");
			oos = new ObjectOutputStream(fos);
			oos.writeObject(localFiles);
			oos.flush();
		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Exception while saving index", e);
		} finally {
			try {
				if (oos != null) {
					oos.close();
				} else if (fos != null) {
					fos.close();
				}
			} catch (Exception e) {
				LOG.log(Level.SEVERE, "Exception while closing index saved", e);				
			}				
		}
	}
	
	private List<Container> getSingleFiles(List<Container> allFiles, int count) {
		ArrayList<Container> result = new ArrayList<Container>();
		HashMap<String, Integer> duplicatedSignatures = new HashMap<String, Integer>();
		for(Container c : allFiles) {
			for(ContentItem item : c.getContent()) {
				String signature = item.getSignature();
				if (!duplicatedSignatures.containsKey(signature)) {
					duplicatedSignatures.put(signature, 1);
				} else {
					duplicatedSignatures.put(signature, duplicatedSignatures.get(signature) + 1);
				}
			}
		}
		
		for(Container c : allFiles) {
			Container copy = new Container(c.getLocation());
			for(ContentItem item : c.getContent()) {
				String signature = item.getSignature();
				if (duplicatedSignatures.get(signature) < count)
					copy.addItem(item);
			}
			if (copy.getSize() > 0)
				result.add(copy);
		}

		return result;		
	}
	
	public List<Container> getSingleFiles(int count) {
		return getSingleFiles(getAllFiles(), count);
	}
	
	public CopyOperation randomCopyOperation(int count) {
		List<Container> allFiles = getAllFiles();
		List<Container> singleFiles = getSingleFiles(allFiles, count);
		if (singleFiles.size() == 0) return null;
		Container source = randomElement(singleFiles);

		ContentItem[] items = source.getContent();
		if (items.length == 0) return null;
		ContentItem item = randomElement(items);

		List<Container> containers = new ArrayList<Container>();
		for(Container c : allFiles) {
			if (c.containsItem(item)) continue;
			containers.add(c);
		}
		if (containers.size() == 0) return null;
		Container destination = randomElement(containers);

		return new CopyOperation(source.getLocation(), destination.getLocation(), item);
	}
	
	private <T> T randomElement(List<T> elements) {
		return elements.get((int) (Math.random() * elements.size()));
	}

	private <T> T randomElement(T[] elements) {
		return elements[(int) (Math.random() * elements.length)];
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) return true;
		if (! (obj instanceof Repository)) return false;
		Repository other = (Repository) obj;
		return getRepositoryDirectory().equals(other.getRepositoryDirectory());
	}
	
	@Override
	public int hashCode() {
		return getRepositoryDirectory().hashCode();
	}
	
	@Override
	public String toString() {
		return "Repository[" + getRepositoryDirectory() + "]";
	}	
}
