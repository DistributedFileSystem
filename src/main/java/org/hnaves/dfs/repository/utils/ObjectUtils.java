package org.hnaves.dfs.repository.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ObjectUtils {
	private ObjectUtils() {}
	
	public static byte[] serializeObject(Object instance) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(instance);
		oos.flush();
		oos.close();
		baos.close();
		return compress(baos.toByteArray());
	}
	
	public static Object deserializeObject(byte[] data) throws IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(uncompress(data));
		ObjectInputStream ois = new ObjectInputStream(bais);
		Object result;
		try {
			result = ois.readObject();
		} catch(ClassNotFoundException ex) {
			throw new RuntimeException(ex);
		}
		ois.close();
		bais.close();
		return result;
	}
	
    public static byte[] compress(byte[] data) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream zip;
        try {
            zip = new GZIPOutputStream(out);
            zip.write(data);
            zip.flush();
            zip.finish();
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }
        return out.toByteArray();
    }

    public static byte[] uncompress(byte[] data) {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        GZIPInputStream zip;
        try {
            int b;
            zip = new GZIPInputStream(in);
            do {
                b = zip.read();
                if (b == -1) break;
                buffer.write((byte)b);
            } while(true);
            return buffer.toByteArray();
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

	


}
