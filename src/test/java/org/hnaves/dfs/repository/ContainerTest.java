package org.hnaves.dfs.repository;

import java.io.File;

import org.hnaves.dfs.election.Node;

import junit.framework.TestCase;

public class ContainerTest extends TestCase {
	public ContainerTest(String testName) {
		super(testName);
	}
	
	public void testUpdateFiles() throws Exception {
		Node node = new Node();
		Container container = new Container(node);
		container.updateFiles(new File("src/test/resources/repository"));
		assertNotNull(container);
		assertEquals(2, container.getContent().length);
	}
}
