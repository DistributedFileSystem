package org.hnaves.dfs.repository;

import java.io.File;

import junit.framework.TestCase;

public class ContentItemTest extends TestCase {
	public ContentItemTest(String testName) {
		super(testName);
	}
	
	public void testMakeContentItem() throws Exception {
		ContentItem item = ContentItem.makeContentItem(new File("src/test/resources/repository/File1"));
		assertNotNull(item);
	}
}
